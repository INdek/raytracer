use std::default::Default;

use crate::ray::Ray;
use crate::vec3::Vec3;

pub struct Camera {
  pub origin: Vec3,
  pub lower_left_corner: Vec3,
  pub horizontal: Vec3,
  pub vertical: Vec3,
}

impl Camera {
  pub fn new(
    origin: Vec3,
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
  ) -> Camera {
    Camera {
      origin,
      lower_left_corner,
      horizontal,
      vertical,
    }
  }


  pub fn get_ray(&self, u: f32, v: f32) -> Ray {
    Ray::new(self.origin, self.lower_left_corner + self.horizontal * u + self.vertical * v)
  }
}

impl Default for Camera {
  fn default() -> Camera {
    Camera::new(
      Vec3::zero(),
      Vec3::new(-2.0, -1.0, -1.0),
      Vec3::new(4.0, 0.0, 0.0),
      Vec3::new(0.0, 2.0, 0.0),
    )
  }
}