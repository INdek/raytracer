use crate::ray::Ray;
use crate::vec3::Vec3;
use crate::hitable::{Hitable, HitRecord};

pub struct Sphere {
  pub center: Vec3,
  pub radius: f32,
}

impl Sphere {
  pub fn new(center: Vec3, radius: f32) -> Sphere {
    Sphere {
      center,
      radius,
    }
  }
}

impl Hitable for Sphere {
  fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
    let oc = ray.origin - self.center;
    let a = ray.direction.dot(ray.direction);
    let b = oc.dot(ray.direction);
    let c = oc.dot(oc) - self.radius * self.radius;
    let discriminant = b*b - a*c;
    if (discriminant > 0.0) {
      let time = (-b - f32::sqrt(discriminant)) / a;
      if (time < t_max && time > t_min) {
        let hit_pos = ray.position_at(time);
        return Some(HitRecord::new(
          time,
          hit_pos,
          (hit_pos - self.center) / self.radius
        ));
      }

      let time = (-b + f32::sqrt(discriminant)) / a;
      if (time < t_max && time > t_min) {
        let hit_pos = ray.position_at(time);
        return Some(HitRecord::new(
          time,
          hit_pos,
          (hit_pos - self.center) / self.radius
        ));
      }
    }

    None
  }
}