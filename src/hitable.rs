use crate::ray::Ray;
use crate::vec3::Vec3;

#[derive(Clone)]
pub struct HitRecord {
  pub time: f32,
  pub pos: Vec3,
  pub normal: Vec3,
}

impl HitRecord {
  pub fn new(time: f32, pos: Vec3, normal: Vec3) -> HitRecord {
    HitRecord {
      time,
      pos,
      normal,
    }
  }
}

pub trait Hitable {
  fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord>;
}