extern crate rand;

use std::fs::File;
use std::io::prelude::*;
use std::ops::*;
use std::f32;
use rand::random;

mod vec3;
mod ray;
mod sphere;
mod camera;
mod hitable;
mod hit_list;

use crate::ray::Ray;
use crate::vec3::Vec3;
use crate::sphere::Sphere;
use crate::camera::Camera;
use crate::hitable::Hitable;
use crate::hit_list::HitList;

fn random_in_unit_sphere() -> Vec3 {
    let mut p = Vec3::unit_random()
        .mul(2.0)
        .sub(Vec3::one());

    while p.len() * p.len() >= 1.0 {
        p = Vec3::unit_random()
        .mul(2.0)
        .sub(Vec3::one());
    }

    return p;
}

fn ray_color<W: Hitable>(world: &W, ray: &Ray) -> Vec3 {
    let hit = world.hit(ray, 0.001, f32::MAX);

    if let Some(hit) = hit {
        let target = hit.pos
            .add(hit.normal)
            .add(random_in_unit_sphere());

        return ray_color(world, &Ray::new(hit.pos, target - hit.pos)).mul(0.5)
    }

    let ray_dir = ray.direction.unit();
    let t = 0.5 * (ray_dir.y + 1.0);
    (
        Vec3::one() * (1.0 - t)
    )  + (
        Vec3::new(0.5, 0.7, 1.0) * t
    )
}

fn write_ppm<W: Hitable>(camera: Camera, world: W, width: usize, height: usize) -> std::io::Result<()>{
    let mut file = File::create("output.ppm")?;
    file.write_fmt(format_args!("P3\n{} {}\n255\n", width, height))?; // File header

    for y in (0..height).rev() {
        for x in 0..width {


            let mut color = Vec3::zero();
            for ray_n in 0..20 {
                let u = (x as f32 + random::<f32>()) / width as f32;
                let v = (y as f32 + random::<f32>()) / height as f32;

                let ray = camera.get_ray(u, v);
                color += ray_color(&world, &ray);
            }

            color /= 20.0;
            color = Vec3::new(
                f32::sqrt(color.x),
                f32::sqrt(color.y),
                f32::sqrt(color.z),
            );
            color *= 255.99;

            let ir = color.x as usize;
            let ig = color.y as usize;
            let ib = color.z as usize;

            file.write_fmt(format_args!("{} {} {}\n", ir, ig, ib))?;
        }
        println!("Progress: {:.2}%", (y as f32 / height as f32) * 100.0);
    }

    Ok(())
}

fn main() {
    println!("Hello, world!");
    let camera = Camera::default();
    let mut world: HitList = Vec::new();

    world.push(Box::new(Sphere::new(Vec3::new(0.0, 0.0, -1.0), 0.5)));
    world.push(Box::new(Sphere::new(Vec3::new(0.0, -100.5, -1.0), 100.0)));

    write_ppm(camera, world, 1920, 1080).unwrap();
}
