use std::f32;
use std::ops;
use std::ops::*;
use rand;

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Vec3 {
  pub x: f32,
  pub y: f32,
  pub z: f32,
}

impl Vec3 {
  pub fn new(x: f32, y: f32, z: f32) -> Vec3 {
    Vec3 {
      x,
      y,
      z,
    }
  }

  pub fn zero() -> Vec3 {
    Vec3 {
      x: 0.0,
      y: 0.0,
      z: 0.0,
    }
  }

  pub fn one() -> Vec3 {
    Vec3 {
      x: 1.0,
      y: 1.0,
      z: 1.0,
    }
  }

  pub fn unit_random() -> Vec3 {
    Vec3 {
      x: rand::random(),
      y: rand::random(),
      z: rand::random(),
    }
  }

  pub fn from_value<I: Into<f32>>(val: I) -> Vec3 {
    let f = val.into();
    Vec3 {
      x: f,
      y: f,
      z: f,
    }
  }

  pub fn len(&self) -> f32 {
    f32::sqrt(
      (self.x * self.x) +
      (self.y * self.y) +
      (self.z * self.z)
    )
  }

  pub fn unit(&self) -> Vec3 {
    let k = 1.0 / self.len();
    *self * Vec3::from_value(k)
  }

  pub fn dot(&self, other: Vec3) -> f32 {
    self.x * other.x +
    self.y * other.y +
    self.z * other.z
  }

  pub fn cross(&self, other: Vec3) -> Vec3 {
    Vec3 {
      x: self.y * other.z - self.z * other. y,
      y: -(self.x * other.z - self.z * other.x),
      z: (self.x * other.y - self.y * other.x),
    }
  }
}

impl ops::Add for Vec3 {
    type Output = Vec3;

    fn add(self, other: Vec3) -> Vec3 {
      Vec3 {
        x: self.x + other.x,
        y: self.y + other.y,
        z: self.z + other.z
      }
    }
}

impl ops::AddAssign for Vec3 {
  fn add_assign(&mut self, other: Vec3) {
    *self = self.add(other);
  }
}

impl ops::Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, other: Vec3) -> Vec3 {
      Vec3 {
        x: self.x - other.x,
        y: self.y - other.y,
        z: self.z - other.z
      }
    }
}

impl ops::SubAssign for Vec3 {
  fn sub_assign(&mut self, other: Vec3) {
    *self = self.sub(other);
  }
}

impl ops::Mul for Vec3 {
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Vec3 {
      Vec3 {
        x: self.x * other.x,
        y: self.y * other.y,
        z: self.z * other.z
      }
    }
}

impl ops::MulAssign for Vec3 {
  fn mul_assign(&mut self, other: Vec3) {
    *self = self.mul(other);
  }
}

impl ops::Div for Vec3 {
    type Output = Vec3;

    fn div(self, other: Vec3) -> Vec3 {
      Vec3 {
        x: self.x / other.x,
        y: self.y / other.y,
        z: self.z / other.z
      }
    }
}

impl ops::DivAssign for Vec3 {
  fn div_assign(&mut self, other: Vec3) {
    *self = self.div(other);
  }
}


impl ops::Add<f32> for Vec3 {
    type Output = Vec3;

    fn add(self, other: f32) -> Vec3 {
      Vec3 {
        x: self.x + other,
        y: self.y + other,
        z: self.z + other
      }
    }
}

impl ops::AddAssign<f32> for Vec3 {
  fn add_assign(&mut self, other: f32) {
    *self = self.add(other);
  }
}

impl ops::Sub<f32> for Vec3 {
    type Output = Vec3;

    fn sub(self, other: f32) -> Vec3 {
      Vec3 {
        x: self.x - other,
        y: self.y - other,
        z: self.z - other
      }
    }
}

impl ops::SubAssign<f32> for Vec3 {
  fn sub_assign(&mut self, other: f32) {
    *self = self.sub(other);
  }
}

impl ops::Mul<f32> for Vec3 {
    type Output = Vec3;

    fn mul(self, other: f32) -> Vec3 {
      Vec3 {
        x: self.x * other,
        y: self.y * other,
        z: self.z * other
      }
    }
}

impl ops::MulAssign<f32> for Vec3 {
  fn mul_assign(&mut self, other: f32) {
    *self = self.mul(other);
  }
}

impl ops::Div<f32> for Vec3 {
    type Output = Vec3;

    fn div(self, other: f32) -> Vec3 {
      Vec3 {
        x: self.x / other,
        y: self.y / other,
        z: self.z / other
      }
    }
}

impl ops::DivAssign<f32> for Vec3 {
  fn div_assign(&mut self, other: f32) {
    *self = self.div(other);
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_len() {
    let vec = Vec3::new(1.0, 2.0, 2.0);
    assert_eq!(vec.len(), 3.0)
  }

  #[test]
  fn test_comparision() {
    assert_eq!(
      Vec3::new(1.0, 2.0, 3.0),
      Vec3::new(1.0, 2.0, 3.0)
    );
  }

  #[test]
  fn test_unit_vec() {
    let initial_vec = Vec3::new(9.0, 0.0, 0.0);
    let unit_vec = Vec3::new(1.0, 0.0, 0.0);
    assert_eq!(initial_vec.unit(), unit_vec)
  }

  #[test]
  fn test_dot() {
    assert_eq!(
      Vec3::new(1.0, 2.0, 3.0).dot(Vec3::new(4.0, 5.0, 6.0)),
      32.0
    )
  }

  #[test]
  fn test_cross() {
    assert_eq!(
      Vec3::new(1.0, 2.0, 3.0).cross(Vec3::new(4.0, 5.0, 6.0)),
      Vec3::new(-3.0, 6.0, -3.0)
    )
  }

  mod vec3_ops {
    use super::*;

    #[test]
    fn test_add() {
      assert_eq!(
        Vec3::new(9.0, 3.0, 22.0) + Vec3::new(1.0, 3.0, 10.0),
        Vec3::new(10.0, 6.0, 32.0)
      )
    }

    #[test]
    fn test_add_assign() {
      let mut v = Vec3::new(9.0, 3.0, 22.0);
      v += Vec3::new(1.0, 3.0, 10.0);
      assert_eq!(v, Vec3::new(10.0, 6.0, 32.0))
    }

    #[test]
    fn test_sub() {
      assert_eq!(
        Vec3::new(9.0, 3.0, 22.0) - Vec3::new(1.0, 3.0, 10.0),
        Vec3::new(8.0, 0.0, 12.0)
      )
    }

    #[test]
    fn test_sub_assign() {
      let mut v = Vec3::new(9.0, 3.0, 22.0);
      v -= Vec3::new(1.0, 3.0, 10.0);
      assert_eq!(v, Vec3::new(8.0, 0.0, 12.0))
    }

    #[test]
    fn test_div() {
      assert_eq!(
        Vec3::new(9.0, 3.0, 20.0) / Vec3::new(1.0, 3.0, 10.0),
        Vec3::new(9.0, 1.0, 2.0)
      );
    }

    #[test]
    fn test_div_assign() {
      let mut v = Vec3::new(9.0, 3.0, 20.0);
      v /= Vec3::new(1.0, 3.0, 10.0);
      assert_eq!(v, Vec3::new(9.0, 1.0, 2.0))
    }

    #[test]
    fn test_mul() {
      assert_eq!(
        Vec3::new(9.0, 3.0, 20.0) * Vec3::new(1.0, 3.0, 10.0),
        Vec3::new(9.0, 9.0, 200.0)
      )
    }

    #[test]
    fn test_mul_assign() {
      let mut v = Vec3::new(9.0, 3.0, 20.0);
      v *= Vec3::new(1.0, 3.0, 10.0);
      assert_eq!(v, Vec3::new(9.0, 9.0, 200.0))
    }
  }

  mod scalar_ops {
    use super::*;

    #[test]
    fn test_add() {
      assert_eq!(
        Vec3::new(9.0, 3.0, 22.0) + 2.0,
        Vec3::new(11.0, 5.0, 24.0)
      )
    }

    #[test]
    fn test_add_assign() {
      let mut v = Vec3::new(9.0, 3.0, 22.0);
      v += 2.0;
      assert_eq!(v, Vec3::new(11.0, 5.0, 24.0))
    }

    #[test]
    fn test_sub() {
      assert_eq!(
        Vec3::new(9.0, 3.0, 22.0) - 7.0,
        Vec3::new(2.0, -4.0, 15.0)
      )
    }

    #[test]
    fn test_sub_assign() {
      let mut v = Vec3::new(9.0, 3.0, 22.0);
      v -= 7.0;
      assert_eq!(v, Vec3::new(2.0, -4.0, 15.0))
    }

    #[test]
    fn test_div() {
      assert_eq!(
        Vec3::new(9.0, 3.0, 21.0) / 3.0,
        Vec3::new(3.0, 1.0, 7.0)
      )
    }

    #[test]
    fn test_div_assign() {
      let mut v = Vec3::new(9.0, 3.0, 21.0);
      v /= 3.0;
      assert_eq!(v, Vec3::new(3.0, 1.0, 7.0))
    }

    #[test]
    fn test_mul() {
      assert_eq!(
        Vec3::new(9.0, 3.0, 22.0) * 2.0,
        Vec3::new(18.0, 6.0, 44.0)
      )
    }
  }
    #[test]
    fn test_mul_assign() {
      let mut v = Vec3::new(9.0, 3.0, 22.0);
      v *= 2.0;
      assert_eq!(v, Vec3::new(18.0, 6.0, 44.0))
    }


}