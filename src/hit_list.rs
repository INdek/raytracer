use crate::ray::Ray;
use crate::hitable::{Hitable, HitRecord};

pub type HitList = Vec<Box<Hitable>>;

impl Hitable for HitList {
  fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
    self.iter()
      .fold(None, |acc, hitable| {
        let closest_so_far = acc.clone().map(|subhit| subhit.time).unwrap_or(t_max);

        let subhit = hitable.hit(ray, t_min, closest_so_far);
        let subhit_time = subhit.clone().map(|subhit| subhit.time).unwrap_or(t_max);

        if subhit_time < closest_so_far {
          return subhit
        } else {
          return acc
        }
      })
  }
}