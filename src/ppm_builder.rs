use std::io::Write;

pub struct PPMBuilder {
  width: usize,
  height: usize,
}

impl PPMBuilder {
  pub fn new(width: usize, height: usize) -> PPMBuilder {
    PPMBuilder {
      width,
      height
    }
  }

  pub fn build<T: Write>(output: T) -> Result<()> {

  }
}