use super::vec3::Vec3;

pub struct Ray {
  pub origin: Vec3,
  pub direction: Vec3,
}

impl Ray {
  pub fn new(origin: Vec3, direction: Vec3) -> Ray {
    Ray {
      origin,
      direction
    }
  }

  /// Calculates the position of the ray at time `t`
  pub fn position_at(&self, t: f32) -> Vec3 {
    self.origin + ( self.direction * t )
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_point_at() {
    let ray = Ray::new(
      Vec3::new(0.0, 0.0, 0.0),
      Vec3::new(2.0, 2.0, 2.0),
    );
    assert_eq!(ray.position_at(0.5), Vec3::new(1.0, 1.0, 1.0))
  }
}